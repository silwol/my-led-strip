#![no_main]
#![no_std]

use my_led_strip as _; // global logger + panicking-behavior + memory layout
use my_led_strip::ColorIterator;

use core::num::NonZeroUsize;
use embedded_hal::digital::v2::InputPin as _;
use nb::block;
use rgb::{ComponentMap as _, RGB8};
use smart_leds::SmartLedsWrite as _;
use stm32f1xx_hal::{pac, prelude::*, spi::Spi, timer::Timer};

const NUM_LEDS: usize = 60;
const COLOR_ITERATOR_SPEED: usize = 1;

#[derive(Clone, Copy, PartialEq, Eq)]
enum InputState {
    JustTurnedOn,
    UnchangedOn,
    JustTurnedOff,
    UnchangedOff,
}

impl InputState {
    fn process(&mut self, input: bool) {
        *self = match (self.is_on(), input) {
            (false, true) => Self::JustTurnedOn,
            (true, true) => Self::UnchangedOn,
            (true, false) => Self::JustTurnedOff,
            (false, false) => Self::UnchangedOff,
        }
    }

    fn is_on(&self) -> bool {
        matches!(*self, Self::JustTurnedOn | Self::UnchangedOn)
    }

    fn is_off(&self) -> bool {
        matches!(*self, Self::JustTurnedOff | Self::UnchangedOff)
    }
}

#[cortex_m_rt::entry]
fn main() -> ! {
    defmt::info!("Starting up…");

    let cp = cortex_m::Peripherals::take().unwrap();
    let dp = pac::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    let clocks = rcc
        .cfgr
        .use_hse(8.mhz())
        .sysclk(48.mhz())
        .pclk1(12.mhz())
        .freeze(&mut flash.acr);

    let mut timer =
        Timer::syst(cp.SYST, &clocks).start_count_down(96.hz());

    let mut afio = dp.AFIO.constrain(&mut rcc.apb2);
    let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);

    let sck = gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl);
    let miso = gpioa.pa6;
    let mosi = gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl);

    let spi = Spi::spi1(
        dp.SPI1,
        (sck, miso, mosi),
        &mut afio.mapr,
        ws2812_spi::MODE,
        2400_u32.khz(),
        clocks,
        &mut rcc.apb2,
    );

    let input_power = gpioa.pa9.into_pull_up_input(&mut gpioa.crh);
    let mut input_power_state = if input_power.is_low().unwrap() {
        InputState::UnchangedOn
    } else {
        InputState::UnchangedOff
    };

    let input_color = gpioa.pa10.into_pull_up_input(&mut gpioa.crh);
    let mut input_color_state = if input_color.is_low().unwrap() {
        InputState::UnchangedOn
    } else {
        InputState::UnchangedOff
    };

    let input_mode = gpioa.pa11.into_pull_up_input(&mut gpioa.crh);
    let mut input_mode_state = if input_mode.is_low().unwrap() {
        InputState::UnchangedOn
    } else {
        InputState::UnchangedOff
    };

    let mut render_buffer = [0u8; 12 * NUM_LEDS + 20];

    let mut rgbleds =
        ws2812_spi::prerendered::Ws2812::new(spi, &mut render_buffer);
    let mut knightrider_effect = KnightRiderEffect::new(
        NonZeroUsize::new(NUM_LEDS).unwrap(),
        NonZeroUsize::new(2000).unwrap(),
    );
    let mut single_color_effect =
        SingleColorEffect::new(NonZeroUsize::new(NUM_LEDS).unwrap());
    let mut rainbow_color_effect =
        RainbowColorEffect::new(NonZeroUsize::new(NUM_LEDS).unwrap());

    defmt::info!("Clearing LEDs");

    rgbleds
        .write(
            core::iter::repeat(RGB8 { r: 0, g: 0, b: 0 }).take(NUM_LEDS),
        )
        .unwrap();
    block!(timer.wait()).unwrap();

    defmt::info!("Running.");

    let mut is_on = false;
    let mut color_iterating = false;

    enum CurrentEffect {
        SingleColor255,
        SingleColor127,
        SingleColor063,
        SingleColor031,
        SingleColor015,
        SingleColor007,
        SingleColor003,
        Knightrider,
        RainbowColor,
    }

    impl CurrentEffect {
        fn switch_next(&mut self) {
            *self = match *self {
                Self::SingleColor255 => Self::SingleColor127,
                Self::SingleColor127 => Self::SingleColor063,
                Self::SingleColor063 => Self::SingleColor031,
                Self::SingleColor031 => Self::SingleColor015,
                Self::SingleColor015 => Self::SingleColor007,
                Self::SingleColor007 => Self::SingleColor003,
                Self::SingleColor003 => Self::Knightrider,
                Self::Knightrider => Self::RainbowColor,
                Self::RainbowColor => Self::SingleColor255,
            };
        }
    }

    let mut current_effect = CurrentEffect::Knightrider;

    loop {
        input_power_state.process(input_power.is_low().unwrap());
        match input_power_state {
            InputState::JustTurnedOn => {
                is_on = !is_on;
                if is_on {
                    current_effect = CurrentEffect::SingleColor255;
                    color_iterating = false;
                }
            }
            InputState::JustTurnedOff => {
                rgbleds
                    .write(
                        core::iter::repeat(RGB8 { r: 0, g: 0, b: 0 })
                            .take(NUM_LEDS),
                    )
                    .unwrap();
                block!(timer.wait()).unwrap();
            }
            InputState::UnchangedOn | InputState::UnchangedOff => {}
        }

        input_color_state.process(input_color.is_low().unwrap());
        if input_color_state == InputState::JustTurnedOn {
            color_iterating = !color_iterating;
        }

        input_mode_state.process(input_mode.is_low().unwrap());
        if input_mode_state == InputState::JustTurnedOn {
            current_effect.switch_next();
            knightrider_effect.reset();
            single_color_effect.reset();
            rainbow_color_effect.reset();
        }

        if is_on {
            knightrider_effect.progress();
            single_color_effect.progress();
            rainbow_color_effect.progress();

            match current_effect {
                CurrentEffect::Knightrider => {
                    rgbleds.write(knightrider_effect.leds_iter()).unwrap();
                }
                CurrentEffect::SingleColor255 => {
                    single_color_effect.brightness = 255;
                    rgbleds
                        .write(single_color_effect.leds_iter())
                        .unwrap();
                }
                CurrentEffect::SingleColor127 => {
                    single_color_effect.brightness = 127;
                    rgbleds
                        .write(single_color_effect.leds_iter())
                        .unwrap();
                }
                CurrentEffect::SingleColor063 => {
                    single_color_effect.brightness = 63;
                    rgbleds
                        .write(single_color_effect.leds_iter())
                        .unwrap();
                }
                CurrentEffect::SingleColor031 => {
                    single_color_effect.brightness = 31;
                    rgbleds
                        .write(single_color_effect.leds_iter())
                        .unwrap();
                }
                CurrentEffect::SingleColor015 => {
                    single_color_effect.brightness = 15;
                    rgbleds
                        .write(single_color_effect.leds_iter())
                        .unwrap();
                }
                CurrentEffect::SingleColor007 => {
                    single_color_effect.brightness = 7;
                    rgbleds
                        .write(single_color_effect.leds_iter())
                        .unwrap();
                }
                CurrentEffect::SingleColor003 => {
                    single_color_effect.brightness = 3;
                    rgbleds
                        .write(single_color_effect.leds_iter())
                        .unwrap();
                }
                CurrentEffect::RainbowColor => {
                    rgbleds
                        .write(rainbow_color_effect.leds_iter())
                        .unwrap();
                }
            }
        }
        if color_iterating {
            knightrider_effect.iterate_color();
            single_color_effect.iterate_color();
            rainbow_color_effect.iterate_color();
        }

        block!(timer.wait()).unwrap();
    }
}

trait Effect {
    type Iter: Iterator<Item = RGB8>;

    fn reset(&mut self);
    fn progress(&mut self);
    fn iterate_color(&mut self);
    fn leds_iter(&self) -> Self::Iter;
}

struct KnightRiderEffect {
    num_leds: NonZeroUsize,
    current_iteration: usize,
    overall_iterations: NonZeroUsize,
    lightcone_width: NonZeroUsize,
    color_iterator: ColorIterator,
}

impl KnightRiderEffect {
    pub fn new(
        num_leds: NonZeroUsize,
        overall_iterations: NonZeroUsize,
    ) -> Self {
        assert!(
            overall_iterations.get()
                > num_leds.get().checked_mul(10).unwrap()
        );
        Self {
            num_leds,
            current_iteration: 0usize,
            overall_iterations,
            lightcone_width: NonZeroUsize::new(
                overall_iterations.get() / 10,
            )
            .unwrap(),
            color_iterator: ColorIterator::new(),
        }
    }
}

impl Effect for KnightRiderEffect {
    type Iter = KnightRiderEffectIterator;

    fn reset(&mut self) {
        self.current_iteration = 0;
        self.color_iterator.reset();
    }

    fn progress(&mut self) {
        self.current_iteration += 1;
        self.current_iteration %= self.overall_iterations.get();
    }

    fn iterate_color(&mut self) {
        self.color_iterator.nth(COLOR_ITERATOR_SPEED);
    }

    fn leds_iter(&self) -> KnightRiderEffectIterator {
        KnightRiderEffectIterator {
            current_led: 0usize,
            num_leds: self.num_leds,
            current_iteration: self.current_iteration,
            overall_iterations: self.overall_iterations,
            lightcone_width: self.lightcone_width,
            base_color: self.color_iterator.current().clone(),
        }
    }
}

struct KnightRiderEffectIterator {
    current_led: usize,
    num_leds: NonZeroUsize,
    current_iteration: usize,
    overall_iterations: NonZeroUsize,
    lightcone_width: NonZeroUsize,
    base_color: RGB8,
}

impl Iterator for KnightRiderEffectIterator {
    type Item = RGB8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current_led == self.num_leds.get() {
            None
        } else {
            let x_in_iteration_scale = self.current_led
                * self.overall_iterations.get()
                / self.num_leds.get();
            self.current_led += 1;

            let distance = if x_in_iteration_scale > self.current_iteration
            {
                x_in_iteration_scale - self.current_iteration
            } else {
                self.current_iteration - x_in_iteration_scale
            };

            let lightcone_width = self.lightcone_width.get();

            let distance = if distance
                > (self.overall_iterations.get() - lightcone_width)
            {
                self.overall_iterations.get() - distance
            } else {
                distance
            };

            if distance > lightcone_width {
                Some(RGB8 { r: 0, g: 0, b: 0 })
            } else {
                let brightness = ((lightcone_width - distance) * 50
                    / lightcone_width)
                    as u8;
                let base_color = self.base_color.clone();
                let effective_color =
                    base_color.with_brightness(brightness);

                Some(effective_color)
            }
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.current_led, Some(self.num_leds.get()))
    }
}

struct RainbowColorEffect {
    color_iterator: ColorIterator,
    num_leds: NonZeroUsize,
}

impl RainbowColorEffect {
    pub fn new(num_leds: NonZeroUsize) -> Self {
        Self {
            color_iterator: ColorIterator::default(),
            num_leds,
        }
    }
}

impl Effect for RainbowColorEffect {
    type Iter = RainbowColorEffectIterator;

    fn reset(&mut self) {
        self.color_iterator.reset();
    }

    fn progress(&mut self) {}

    fn iterate_color(&mut self) {
        self.color_iterator.nth(COLOR_ITERATOR_SPEED);
    }

    fn leds_iter(&self) -> RainbowColorEffectIterator {
        RainbowColorEffectIterator {
            current_led_index: 0,
            color_iterator: self.color_iterator.clone(),
            num_leds: self.num_leds,
        }
    }
}

struct RainbowColorEffectIterator {
    color_iterator: ColorIterator,
    num_leds: NonZeroUsize,
    current_led_index: usize,
}

impl Iterator for RainbowColorEffectIterator {
    type Item = RGB8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current_led_index == self.num_leds.get() {
            None
        } else {
            self.current_led_index += 1;
            self.color_iterator.nth(30)
        }
    }
}

struct SingleColorEffect {
    color_iterator: ColorIterator,
    brightness: u8,
    num_leds: NonZeroUsize,
}

impl SingleColorEffect {
    pub fn new(num_leds: NonZeroUsize) -> Self {
        Self {
            num_leds,
            brightness: 255,
            color_iterator: ColorIterator::default(),
        }
    }
}

impl Effect for SingleColorEffect {
    type Iter = SingleColorEffectIterator;

    fn progress(&mut self) {}

    fn reset(&mut self) {
        self.color_iterator.reset();
    }

    fn iterate_color(&mut self) {
        self.color_iterator.nth(COLOR_ITERATOR_SPEED);
    }

    fn leds_iter(&self) -> SingleColorEffectIterator {
        let color = self
            .color_iterator
            .current()
            .with_brightness(self.brightness);

        SingleColorEffectIterator {
            current_led: 0usize,
            num_leds: self.num_leds,
            color,
        }
    }
}

struct SingleColorEffectIterator {
    color: RGB8,
    current_led: usize,
    num_leds: NonZeroUsize,
}

impl Iterator for SingleColorEffectIterator {
    type Item = RGB8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current_led == self.num_leds.get() {
            None
        } else {
            self.current_led += 1;
            Some(self.color)
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.current_led, Some(self.num_leds.get()))
    }
}

trait WithBrightness {
    fn with_brightness(self, brightness: u8) -> Self;
}

impl WithBrightness for RGB8 {
    fn with_brightness(self, brightness: u8) -> Self {
        self.map(|c| (c as u16 * brightness as u16 / 255) as u8)
    }
}
