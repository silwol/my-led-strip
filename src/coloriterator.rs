use core::iter::Iterator;
use rgb::RGB8;

const GRAY_VALUES: [u8; 7] =
    [0b100, 0b101, 0b111, 0b110, 0b010, 0b011, 0b001];

const RED_INDEX: usize = 2;
const GREEN_INDEX: usize = 1;
const BLUE_INDEX: usize = 0;

#[derive(Clone)]
pub struct ColorIterator {
    target_gray_index: usize,
    change_r: ColorChannelAction,
    change_g: ColorChannelAction,
    change_b: ColorChannelAction,
    current_color: RGB8,
    target_color: RGB8,
}

impl Iterator for ColorIterator {
    type Item = RGB8;

    fn next(&mut self) -> Option<RGB8> {
        self.change_r.apply(&mut self.current_color.r);
        self.change_g.apply(&mut self.current_color.g);
        self.change_b.apply(&mut self.current_color.b);

        if self.current_color == self.target_color {
            let current_graycode = GRAY_VALUES[self.target_gray_index];
            self.target_gray_index =
                (self.target_gray_index + 1) % GRAY_VALUES.len();
            let target_graycode = GRAY_VALUES[self.target_gray_index];

            self.change_r = Self::color_channel_action(
                RED_INDEX,
                current_graycode,
                target_graycode,
            );
            self.change_g = Self::color_channel_action(
                GREEN_INDEX,
                current_graycode,
                target_graycode,
            );
            self.change_b = Self::color_channel_action(
                BLUE_INDEX,
                current_graycode,
                target_graycode,
            );
            self.target_color = Self::graycode_to_color(target_graycode);
        }

        Some(self.current_color)
    }
}

impl Default for ColorIterator {
    fn default() -> Self {
        ColorIterator::new()
    }
}

impl ColorIterator {
    pub fn new() -> Self {
        let target_gray_index = 1;
        let current_graycode = GRAY_VALUES[0];
        let target_graycode = GRAY_VALUES[target_gray_index];

        let change_r = Self::color_channel_action(
            RED_INDEX,
            current_graycode,
            target_graycode,
        );
        let change_g = Self::color_channel_action(
            GREEN_INDEX,
            current_graycode,
            target_graycode,
        );
        let change_b = Self::color_channel_action(
            BLUE_INDEX,
            current_graycode,
            target_graycode,
        );

        Self {
            target_gray_index,
            change_r,
            change_g,
            change_b,
            current_color: Self::graycode_to_color(current_graycode),
            target_color: Self::graycode_to_color(target_graycode),
        }
    }

    pub fn reset(&mut self) {
        let target_gray_index = 1;
        let current_graycode = GRAY_VALUES[0];
        let target_graycode = GRAY_VALUES[target_gray_index];

        let change_r = Self::color_channel_action(
            RED_INDEX,
            current_graycode,
            target_graycode,
        );
        let change_g = Self::color_channel_action(
            GREEN_INDEX,
            current_graycode,
            target_graycode,
        );
        let change_b = Self::color_channel_action(
            BLUE_INDEX,
            current_graycode,
            target_graycode,
        );

        self.target_gray_index = target_gray_index;
        self.change_r = change_r;
        self.change_g = change_g;
        self.change_b = change_b;
        self.current_color = Self::graycode_to_color(current_graycode);
        self.target_color = Self::graycode_to_color(target_graycode);
    }

    pub fn current(&self) -> RGB8 {
        self.current_color
    }

    fn graycode_to_color(graycode: u8) -> RGB8 {
        RGB8 {
            r: 255 * ((graycode >> RED_INDEX) & 1),
            g: 255 * ((graycode >> GREEN_INDEX) & 1),
            b: 255 * ((graycode >> BLUE_INDEX) & 1),
        }
    }

    fn color_channel_action(
        channel_index: usize,
        old: u8,
        new: u8,
    ) -> ColorChannelAction {
        ColorChannelAction::for_change(
            ((old >> channel_index) & 1) == 1,
            ((new >> channel_index) & 1) == 1,
        )
    }
}

#[derive(Clone, PartialEq, Eq)]
enum ColorChannelAction {
    Increase,
    Keep,
    Decrease,
}

impl ColorChannelAction {
    fn for_change(from: bool, to: bool) -> Self {
        match (from, to) {
            (false, true) => Self::Increase,
            (true, false) => Self::Decrease,
            (true, true) | (false, false) => Self::Keep,
        }
    }

    fn calculate(&self, input: u8) -> u8 {
        match *self {
            Self::Increase => input.saturating_add(1),
            Self::Keep => input,
            Self::Decrease => input.saturating_sub(1),
        }
    }

    fn apply(&self, value: &mut u8) {
        *value = self.calculate(*value);
    }
}
