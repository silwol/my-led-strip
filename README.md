# Control a RGB LED strip using a BluePill board

This is a small hobby project of mine for building small controller boxes
with a STM32 BluePill board, and attached to it RGB LED strips based on
WS2812 ICs.

It has several different modes that can be cycled through by pressing the
**Mode** button, it smoothly transitions between different colors within
each mode which an be paused by pressing the **Color** button, and it can
be put into standby mode by pressing the **Power** button.

Please be aware that I'm a layman with regard to electronics, so it is very
likely that I violate some well-known rule in this project. Feel free to
inform me about such findings by
[opening an issue](https://gitlab.com/silwol/my-led-strip/-/issues).

## I/O mapping

The ASCII art is compatible with
[svgbob](https://github.com/ivanceras/svgbob), paste it into
[svgbob-editor](https://ivanceras.github.io/svgbob-editor/) if you want to
see it rendered beautifully.

I simply supply the board with power from a USB charger thrugh the
Micro-USB socket.

```svgbob
                               .----------------.
                               | STM32 BluePill |
                               |o B12        G o|
                               |o B13        G o|---------.
                               |o B14     3.3V o|         |
                               |o B15        R o|         |
                               |o A8       B11 o|         |
   .---------------------------|o A9       B10 o|         |     LED STRIP
   |        .------------------|o A10       B1 o|         |     .---------- - -
   |        |        .---------|o A11       B0 o|         '-----|o GND        /
   |        |        |         |o A12       A7 o|---------------|o D IN       \
   |        |        |         |o A15       A6 o|         .-----|o 5V         /
   |        |        |         |o B3        A5 o|         |     '---------- - -
   |        |        |         |o B4        A4 o|         |
   |        |        |         |o B5        A3 o|         |
   |        |        |         |o B6        A2 o|         |
   |        |        |         |o B7        A1 o|         |
   o        o        o         |o B8        A0 o|         |
|-\      |-\      |-\          |o B9       C15 o|         |
   \        \        \     .---|o 5V       C14 o|         |
   |        |        |     | .-|o G        C13 o|         |
   '--------+--------+-------' |o 3.3V      VB o|         |
standby  color    mode     |   '----------------'         |
button   button   button   |                              |
                           '------------------------------'
```

## Other details

Bootstrapped using the Knurling-RS
[app-template](https://github.com/knurling-rs/app-template/).

## License

Licensed under the Affero General Public License, Version 3
([LICENSE-AGPLV3](LICENSE-AGPLV3)).
